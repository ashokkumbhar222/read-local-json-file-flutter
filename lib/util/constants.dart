import 'package:flutter/material.dart';

bool isLoggedIn = false;
String email = '';
// List restaurantsData = [];

final kFieldLblStyle = TextStyle(
  color: Colors.green,
  fontWeight: FontWeight.bold
);

final kFieldBoxDecorationStyle = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(5.0),
  boxShadow: [
    BoxShadow(
      color: Colors.green[50],
      blurRadius: 6.0,
      offset: Offset(0, 2)
    ),
  ]
);

final kBoxDecorationStyle = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(5.0),
  // boxShadow: [
  //   BoxShadow(
  //     color: Colors.green[100],
  //     blurRadius: 6.0,
  //     offset: Offset(0, 3)
  //   ),
  // ]
);
