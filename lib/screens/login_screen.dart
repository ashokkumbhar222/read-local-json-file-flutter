import 'package:LoginDemo/screens/home_screen.dart';
import 'package:LoginDemo/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final emailTFController = TextEditingController();
  final passwordTFController = TextEditingController();

  // bool isLoggedIn = false;
  // String email = '';

  @override
  void initState() {
    super.initState();
    emailTFController.addListener(_emailTFValue);
    passwordTFController.addListener(_passwordTFValue);
    autoLogIn();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    emailTFController.dispose();
    passwordTFController.dispose();
    super.dispose();
  }

  _emailTFValue() {
    // print("Email: ${emailTFController.text}");
  }

  _passwordTFValue() {
    // print("Password: ${passwordTFController.text}");
  }

  void autoLogIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String emailId = prefs.getString('emailId');
    
    if (emailId != null) {
      setState(() {
        isLoggedIn = true;
        email = emailId;
      });
      Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
      return;
    }
  }

  onTapLoginButton() async {
    if (emailTFController.text.isEmpty) {
      print("Please enter your email");
      return;
    }
    if (passwordTFController.text.isEmpty) {
      print('Please enter your password');
      return;
    }
    print('Login Success');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('username', emailTFController.text);

    setState(() {
      email = emailTFController.text;
      isLoggedIn = true;
    });

    emailTFController.clear();
    Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
    // Navigator.pushReplacementNamed(context, "/homescreen");
  }

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Email', style: kFieldLblStyle),
        SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFieldBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: emailTFController,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(Icons.email, color: Colors.green),
              hintText: 'Please enter your email',
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('Password', style: kFieldLblStyle),
        SizedBox(
          height: 10,
        ),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kFieldBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            obscureText: true,
            controller: passwordTFController,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(Icons.lock, color: Colors.green),
              hintText: 'Please enter your passowrd',
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLoginButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5,
        onPressed: () => onTapLoginButton(),
        padding: EdgeInsets.all(15.0),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        color: Colors.green,
        child: Text(
          'LOGIN',
          style: TextStyle(
              color: Colors.white,
              letterSpacing: 1.6,
              fontSize: 18.0,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Stack(children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Color(0xFFF),
            ),
          ),
          Container(
            height: double.infinity,
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 40.0,
                vertical: 160.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Sign In',
                    style: TextStyle(
                      color: Colors.green,
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  _buildEmailTF(),
                  SizedBox(
                    height: 30,
                  ),
                  _buildPasswordTF(),
                  _buildLoginButton(),
                ],
              ),
            ),
          ),
        ]),
      ),
    ));
  }
}
