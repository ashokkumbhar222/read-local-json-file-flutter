import 'package:LoginDemo/util/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var restaurantsData = [];
  Future _future;
  Future<String> loadJson() async =>
      await rootBundle.loadString('assets/restaurants.json');

  @override
  void initState() {
    _future = loadJson();
    super.initState();
  }

  onTapLoginButton() async {
    print("Logout");
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('username', null);

    setState(() {
      email = '';
      isLoggedIn = false;
    });
    Navigator.pop(context);
  }

  _sortAtoZ() async {
    print('Sort A - Z');

    restaurantsData.sort((a, b) {
      return a['name']
          .toString()
          .toLowerCase()
          .compareTo(b['name'].toString().toLowerCase());
    });
    print("object: ${restaurantsData}");
    print("Length: ${restaurantsData.length}");
    setState(() {
      // restaurantsData = restaurantsData
    });
  }

  _sortZtoA() {
    // _future = sortZtoA();
    // setState(() {
      restaurantsData.sort((a, b) {
        return b['name']
            .toString()
            .toLowerCase()
            .compareTo(a['name'].toString().toLowerCase());
      });
    // });
    print("object: ${restaurantsData}");
    print("Length: ${restaurantsData.length}");
  }

  // Future<Null> sortZtoA() async =>
  //     await restaurantsData.sort((a, b) {
  //       return b['name']
  //           .toString()
  //           .toLowerCase()
  //           .compareTo(a['name'].toString().toLowerCase());
  //     });

  Widget _subTitleContainer(restaurantsData, index) {
    return Container(
        alignment: Alignment.centerLeft,
        decoration: kBoxDecorationStyle,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Neighborhood: ' +
                    restaurantsData[index]["neighborhood"] +
                    '\nAddress: ' +
                    restaurantsData[index]["address"] +
                    '\nDistance: ' +
                    restaurantsData[index]["distance"].toString(),
              ),
              SizedBox(height: 10.0),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(8.0),
                decoration: kFieldBoxDecorationStyle,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        'Reviews:',
                        style: kFieldLblStyle,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(restaurantsData[index]['reviews'][0]['name']
                              .toString()),
                          Text(restaurantsData[index]['reviews'][0]['date']
                              .toString()),
                          Text('(' +
                              restaurantsData[index]['reviews'][0]['rating']
                                  .toString() +
                              ')'),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(restaurantsData[index]['reviews'][1]['name']
                              .toString()),
                          Text(restaurantsData[index]['reviews'][1]['date']
                              .toString()),
                          Text('(' +
                              restaurantsData[index]['reviews'][1]['rating']
                                  .toString() +
                              ')'),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(restaurantsData[index]['reviews'][2]['name']
                              .toString()),
                          Text(restaurantsData[index]['reviews'][2]['date']
                              .toString()),
                          Text('(' +
                              restaurantsData[index]['reviews'][2]['rating']
                                  .toString() +
                              ')'),
                        ],
                      ),
                    ]),
              )
            ]));
  }

  Widget _bodyContainer() {
    return Center(
        child: FutureBuilder(
      future: _future,
      // DefaultAssetBundle.of(context).loadString("assets/restaurants.json"),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return CircularProgressIndicator();
        }
        var data = json.decode(snapshot.data.toString());
        restaurantsData = data["restaurants"];
        return ListView.builder(
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(15.0),
          itemCount: restaurantsData == null ? 0 : restaurantsData.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
                child: ListTile(
              leading: CircleAvatar(
                  backgroundImage: AssetImage(
                      'assets/images/' + restaurantsData[index]["photograph"])),
              title: Text(
                restaurantsData[index]["name"],
                style: kFieldLblStyle,
              ),
              subtitle: _subTitleContainer(restaurantsData, index),
              isThreeLine: true,
              // trailing: CircleAvatar(
              //     backgroundImage: AssetImage('assets/images/' +
              //         restaurantsData[index]["photograph"])),
            ));
          },
          
        );
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Home'),
          backgroundColor: Colors.green,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: GestureDetector(child: Icon(Icons.search, size: 26.0)),
            )
          ],
        ),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark,
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(children: <Widget>[
              Container(
                height: double.infinity,
                // width: double.infinity,
                decoration: BoxDecoration(
                  color: Color(0xFFF),
                ),
              ),
              Container(
                  height: double.infinity,
                  child: Column(children: [
                    Container(
                        height: 50.0,
                        width: double.infinity,
                        padding: EdgeInsets.all(14.0),
                        decoration: kFieldBoxDecorationStyle,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: () => _sortAtoZ(),
                              child: Text(
                                "A-Z",
                                style: TextStyle(fontSize: 15.0),
                              ),
                            ),
                            RaisedButton(
                              onPressed: () async => _sortZtoA(),
                              child: Text(
                                "Z-A",
                                style: TextStyle(fontSize: 15.0),
                              ),
                            )
                          ],
                        )),
                    Expanded(child: _bodyContainer())
                  ])),
            ]),
          ),
        ));
  }
}
